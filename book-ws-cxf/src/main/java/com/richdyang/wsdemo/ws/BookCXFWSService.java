package com.richdyang.wsdemo.ws;

import com.richdyang.wsdemo.bean.Book;
import com.richdyang.wsdemo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@Component("bookCXFWSService")
@WebService(targetNamespace = "http://richdyang.com/wsdemo/book")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class BookCXFWSService {

    @Autowired
    private BookService bookService;

    @WebMethod
    public Book[] listBooks() {
        return bookService.listBooks();
    }

    @WebMethod
    public Book findBook(String isbn) {
        return bookService.findBook(isbn);
    }

}
