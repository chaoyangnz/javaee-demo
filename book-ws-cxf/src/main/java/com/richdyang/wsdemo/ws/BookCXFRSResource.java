package com.richdyang.wsdemo.ws;

import com.richdyang.wsdemo.bean.Book;
import com.richdyang.wsdemo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Component("bookCXFRSResource")
@Path("/BookCXFRSResource")
@Produces({"application/xml", "application/json"})
public class BookCXFRSResource {

    @Autowired
    private BookService bookService;

    @GET
    @Path("/")
    public Book[] listBooks() {
        return bookService.listBooks();
    }

    @GET
    @Path("/{isbn}")
    public Book findBook(@PathParam("isbn") String isbn) {
        return bookService.findBook(isbn);
    }
}
