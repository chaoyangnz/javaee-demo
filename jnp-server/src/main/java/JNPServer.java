
import org.jnp.server.Main;
import org.jnp.server.NamingBeanImpl;

import javax.naming.Context;
import java.net.InetAddress;

/**
 * Created by Richard on 18/06/15.
 */
public class JNPServer {

    public static void main(String[] args) throws Exception {
        System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");

        //start the naming info bean
        final NamingBeanImpl _naming = new NamingBeanImpl();
        _naming.start();

        //start the jnp serve
        final Main _server = new Main();
        _server.setNamingInfo(_naming);
        _server.setPort(1099);
        _server.setBindAddress("192.168.1.8");
        _server.start();
    }
}
