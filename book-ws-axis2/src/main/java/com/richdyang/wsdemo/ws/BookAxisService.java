package com.richdyang.wsdemo.ws;

import com.richdyang.wsdemo.bean.Book;
import com.richdyang.wsdemo.service.BookService;
import org.apache.axiom.om.*;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * This is a sample axis2 service created by the AXIOM way
 */
@Component("bookAxisService")
public class BookAxisService {

    @Autowired
    private BookService bookService;

    private final static String WS_NS = "http://richdyang.com/wsdemo/book";
    private Logger logger = Logger.getLogger(getClass());

    public OMElement listBooks(OMElement requestEl) {
        OMFactory factory = OMAbstractFactory.getOMFactory();
        OMNamespace tns = factory.createOMNamespace(WS_NS, "tns");
        OMElement responseEl = factory.createOMElement("listBooksResponse", tns);
        for (Book book : bookService.listBooks()) {

            OMElement titleEl = factory.createOMElement("title", tns);
            titleEl.addChild(factory.createOMText(titleEl, book.getTitle()));
            OMElement isbnEl = factory.createOMElement("isbn", tns);
            isbnEl.addChild(factory.createOMText(isbnEl, book.getIsbn()));
            OMElement authorEl = factory.createOMElement("author", tns);
            authorEl.addChild(factory.createOMText(authorEl, book.getAuthor()));

            OMElement bookEl = factory.createOMElement("book", tns);
            bookEl.addChild(titleEl);
            bookEl.addChild(isbnEl);
            bookEl.addChild(authorEl);

            responseEl.addChild(bookEl);
        }

        logger.info(requestEl);
        logger.info(responseEl);

        return responseEl;
    }
}
