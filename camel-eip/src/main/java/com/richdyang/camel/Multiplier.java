package com.richdyang.camel;

import org.springframework.stereotype.Component;

@Component("multiplier")
public class Multiplier {
    public int multiply(final int originalNumber) {
        return originalNumber * 4;
    }
}
