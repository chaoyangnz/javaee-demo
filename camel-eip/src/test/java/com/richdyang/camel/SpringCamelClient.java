package com.richdyang.camel;

import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.xbean.spring.context.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;

public class SpringCamelClient {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/spring-camel-client.xml");


        // get the camel template for Spring template style sending of messages (= producer)
        ProducerTemplate camelTemplate = context.getBean("camelTemplate", ProducerTemplate.class);

        System.out.println("Invoking the multiply with 678");
        // as opposed to the CamelClientRemoting example we need to define the service URI in this java code
        int response = (Integer)camelTemplate.sendBody("activemq:queue:numbers", ExchangePattern.InOut, 678);
        System.out.println("... the result is: " + response);

        System.exit(0);
    }
}
