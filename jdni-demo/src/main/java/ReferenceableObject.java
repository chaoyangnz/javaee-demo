import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import java.io.Serializable;

/**
 * Created by Richard on 18/06/15.
 */
public class ReferenceableObject implements Referenceable {
    private String user;
    private String pass;
    public ReferenceableObject(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }
    public Reference getReference() throws NamingException {
        Reference ref = new Reference(getClass().getName(), ReferenceableObjectFactory.class.getName(), null);
        ref.add(new StringRefAddr("user", user));
        ref.add(new StringRefAddr("pass", pass));
        return ref;
    }
}
