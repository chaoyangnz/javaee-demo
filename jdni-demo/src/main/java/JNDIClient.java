import javax.naming.*;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;

public class JNDIClient {
    public static void main(String[] args) throws Exception {
        Hashtable<String, String> c = new Hashtable<String, String>();
        c.put("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
        c.put("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");
        c.put("java.naming.provider.url", "jnp://192.168.1.8:1099");


        Context ctx = new InitialContext(c);

        ctx.rebind("test", new String("test"));//rebind(): again bind() if existing
        String s = (String) ctx.lookup("test");//lookup to verify
        System.out.println(s);

//        ctx.rebind("adfdfd", new NonSerializableObject()); // complain

        ReferenceableObject refObj = new ReferenceableObject("wasadmin", "123456");

        ctx.rebind("ss", refObj);
        ReferenceableObject ss = (ReferenceableObject) ctx.lookup("ss");//查找一个接口

        System.out.println(refObj == ss);
    }
}



