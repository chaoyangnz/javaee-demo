import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.StringRefAddr;
import javax.naming.spi.ObjectFactory;
import java.util.Hashtable;

public class ReferenceableObjectFactory implements ObjectFactory {
    public Object getObjectInstance(Object reference, Name name, Context nameCtx, Hashtable<?, ?> environment) throws Exception {
        Reference ref = (Reference) reference;

        StringRefAddr user = (StringRefAddr) ref.get("user");
        StringRefAddr pass = (StringRefAddr) ref.get("pass");

        return new ReferenceableObject((String)user.getContent(), (String)pass.getContent());
    }
}