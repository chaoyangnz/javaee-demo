package com.richdyang.wsdemo.service;

import com.richdyang.wsdemo.bean.Book;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BookService {

    private List<Book> books = new ArrayList<Book>();

    public BookService() {
        Book book = new Book();
        book.setAuthor("Dan Diephouse");
        book.setTitle("Using Axis2");
        book.setIsbn("0123456789");
        books.add(book);

        book = new Book();
        book.setAuthor("Richard Yang");
        book.setTitle("Using Axis2 Another book");
        book.setIsbn("9876543210");
        books.add(book);
    }

    public Book[] listBooks() {
        return books.toArray(new Book[]{});
    }

    public Book findBook(String isbn) {
        for(Book book : books) {
            if (isbn.equals(book.getIsbn()))
                return book;
        }

        return null;
    }
}
